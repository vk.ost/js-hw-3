"use strict";

// 1) Циклы нужны в тех случаях когда нам нужно
//    выполнить какое то одинаковое действие то количество раз, пока не выполниться условие цикла.
//    Например проверка числа, строки или значения на любое условие.
// 2) Цикл for я бы использовал когда известно количество повторений, 
//    while и do while когда неизвестное количество повторений,
//    я бы использовал их для проверки на коректность введеных данных.
// 3) Явное преобразование когда используеться специальная функция или метод для этого. 
//    Неявное преобразование подразумевает использование логических 
//    или арефметических операторов или унарного плюса.

const number = prompt('Enter your number');
if(number<5){
    console.log('Sorry, no numbers');
}else{
    for(let i = 1; i < number; i++){
        if(i%5 === 0){
            console.log(i);
        }
    }
}

let integer;
let checkNumber;
do{
    integer = prompt('Enter your number');
    checkNumber = parseInt(integer);
}while(integer !== checkNumber.toString())
alert('Your number is integer');

let primeNumber1 = prompt('Enter your first number');
let primeNumber2 = prompt('Enter your second number');
while(+primeNumber1 === 0 || isNaN(+primeNumber1) || +primeNumber2 === 0 || isNaN(+primeNumber2)){
    alert('One of your numbers is not valid')
    primeNumber1 = prompt('Enter your first number again');
    primeNumber2 = prompt('Enter your second number again');
}

// Перший варіант
if(primeNumber1 < primeNumber2){
    for(let i = primeNumber1; i<=primeNumber2; i++){
        let check = 0;
        for(let y=1;y<=i;y++){
            if(i%y === 0){
                check++;
            }
        }
        if(check === 2) console.log(i);;
    }
}

// Другій варіант
if(primeNumber1 < primeNumber2){
    for(let i = primeNumber1; i<=primeNumber2; i++){
           if(getPrime(i)){
            console.log(i);
           }
    }
}

function getPrime(a){
    let check = 0;
    for(let i=1; i<=a; i++){
        if(a%i === 0){
            check++;
        }
    }
    if(check === 2) return true;
}
